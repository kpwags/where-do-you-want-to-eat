# WHERE DO YOU WANT TO EAT README #

This project is a little tool to decide where to eat without having to actually decide.

You can view this in action at [http://wheredoyouwanttoeat.xyz](http://wheredoyouwanttoeat.xyz)

### What is this repository for? ###

This repository contains the web files for the site.

### How do I get set up? ###

* Place the files in the web directory
* Put your Google API key at the bottom of index.html where it says GOOGLE_API_KEY

### Contribution guidelines ###

Please feel free to use and enjoy this, but understand it is licensed under GPLv2.

### Developed By ###

Keith Wagner

[http://kpwags.com](http://kpwags.com)